#pragma once

#include <atomic>
#include <mutex>

class Clock{
public:
	Clock() : time_(0){};
	void Increase();
	void Compare(unsigned long long an_time);
	unsigned long long GetTime();
private:
	std::mutex mutex;
	std::atomic<unsigned long long> time_;
};