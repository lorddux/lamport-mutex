#pragma once

#include <sys/file.h>
#include <iostream>
#include <string>
#include <thread>

#include "rpc.h"
#include "clock.h"

class Lamport{
public:
	Lamport(int id, const char* addr, int port, int proc_count, 
			std::vector<char*> addrs, std::vector<int> ports);
	void Alg(const char * filename, int mode, int iters);
private:
	// параллельная обработка запросов других процессов на взятие мьютекса
	void MultiPerm();
	// отправка разрешений на взятие мьютекса и проверка условий (сравниваются отметки времен своего запроса и другого процесса)
	void PermissionToOne(int other_proc_num);
	Clock p_time;
	Proc proc;
	char log_mark[100];
	int id;
	int iters;
	int proc_count;
	int logfd;
	unsigned long long myrectime;
};