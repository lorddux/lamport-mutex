#include "lamport.h"


Lamport::Lamport(int id, const char* addr, int port,
	int proc_count, std::vector<char*> addrs, std::vector<int> ports) : 
	id(id), proc_count(proc_count), proc(addr, port, proc_count, addrs, ports), myrectime(-1) 
{
	std::string log_name = std::to_string(id) + "_log.txt";
    logfd = open(log_name.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0600);
}

void Lamport::PermissionToOne(int other_proc_num){
	// unsigned long long an_time;
	std::pair<unsigned long long, int> request_pair;
	// ожидаем запрос на взятие мьютекса
	while(true){
		request_pair = proc.RecvRequest(other_proc_num);
		if(request_pair.first == 0) {
			return;
		}
		p_time.Compare(request_pair.first);
		p_time.Increase();
		// ожидаем выполнение условий. в случае совпадения времен запроса на взятие смотрим на id процессов
		while(!((myrectime > request_pair.first) || (myrectime == request_pair.first && id > request_pair.second)));
		p_time.Increase();
		// отправляем разрешение на взятие мьютекса
		proc.SendPermission(other_proc_num, p_time.GetTime());
	}
}

void Lamport::MultiPerm(){
	std::vector<std::thread> threads;
    for(int i = 0; i < proc_count; ++i){
        proc.Accept(i);
        threads.emplace_back([&, i] () {
            PermissionToOne(i);
        });
    }

    for(auto& thread : threads) thread.join();
}

void Lamport::Alg(const char * filename, int mode, int iters){
	// Clock p_time;
	// char log_mark[100];

	// создание/открытие/очистка файла логов на запись
	std::string log_name = std::to_string(id) + "_log.txt";
    int logfd = open(log_name.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0600);

    // создание/очистка общего файла
	int fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0600);
	close(fd);

	proc.Listen();
	std::thread t1([&] () {
		MultiPerm();
	});
		
	proc.ConnectToAll();

	std::string n = "";
	int k = 0;
	timeval tv;
	if(mode == 0){
		printf("PID %d\nTake mutex? (y/n)\n", id);
	} else {
		printf("PID %d. Stress mode: %d iterations\n", id, iters);
	}
	while(mode || std::cin >> n){
		if(n != "y" && mode == 0) {
			if(n == "n"){
				printf("PID %d. DONE\n", id);
				break;
			} else {
				printf("Type y or n\n");
			}
		}
		// делаем запросы на взятие мьютекса
		// unsigned long long ac_time = proc.MutexRequest();
		
		p_time.Increase();
		myrectime = p_time.GetTime();
		// логирование запроса на взятиие мьютекса
	    gettimeofday(&tv, 0);
	    int len = sprintf(log_mark, "request PID %d real %ld.%ld log %lld\n",id ,tv.tv_sec, tv.tv_usec, p_time.GetTime());
	    if(write(logfd, log_mark, len) < 0)
	    	throw std::runtime_error("logging error");
	    // конец логирования
		// рассылка запросов на взятие мьютекса
		for(int i = 0; i < proc_count; ++i){
			proc.SendRequest(i, id, myrectime);
		}
		// ожидание ответов от всех процессов
		for(int i = 0; i < proc_count; ++i){
			unsigned long long an_time = proc.RecvPermission(i);
			p_time.Compare(an_time);
		}
		// увеличиваем значение логических часов
		p_time.Increase();
		// мьютекс взят. критическая секция
		unsigned long long ac_time = p_time.GetTime(); // отметка времени взятия мьютекса
		
		// логирование
		gettimeofday(&tv, 0);
	    len = sprintf(log_mark, "acquare PID %d real %ld.%ld log %lld\n", id, tv.tv_sec, tv.tv_usec, ac_time);
	    if(write(logfd, log_mark, len) < 0)
	    	throw std::runtime_error("logging error");
	    // конец логирования

	    if ((fd = open(filename, O_WRONLY | O_APPEND, 0600)) == -1) 
	    	throw std::runtime_error("cannot open file");
	    // проверка flock
	    if(flock(fd, LOCK_EX | LOCK_NB) < 0) 
	    	throw std::runtime_error("flock error");
	    // запись времени взятия мьютекса в файл
	    char mark[70];
	    len = sprintf(mark, "PID %3d acquire time: %12lld", id, ac_time);
	    write(fd, mark, len);

	    unsigned long long rel_time = p_time.GetTime(); // отметка времени освобождения мьютекса
	    // запись времени освобождения мьютекса
	    len = sprintf(mark, "   release time: %12lld\n", rel_time);
	    write(fd, mark, len);

	    // логирование времени освобождения мьютекса
	    gettimeofday(&tv, 0);
	    len = sprintf(log_mark, "release PID %d real %ld.%ld log %lld\n",id ,tv.tv_sec, tv.tv_usec, rel_time);
	    if(write(logfd, log_mark, len) < 0)
	    	throw std::runtime_error("logging error");
	    // конец логорования

	    flock(fd, LOCK_UN);
	    close(fd);
	    // proc.MutexRelease();
	    myrectime = -1; // переполняем свое время запроса, чтобы оно стало больше любого значения

	    if(mode == 0){
			printf("Take mutex? (y/n)\n");
		}
	    if(++k == iters && mode || k > 1000){ // меры предосторожности для случая с бесконечным циклом
	    	printf("PID %d. DONE\n", id);
	    	break;
	    }
	}
	close(logfd);
	proc.CloseSocks();
	t1.join();
}