#include "lamport.h"

#include <gtest/gtest.h>

void f(int id, const char* addr, int port, int proc_count, 
       std::vector<char*> addrs, std::vector<int> ports,
       const char * filename, int mode, int iters)
{
    Lamport lamport(id, addr, port, proc_count, addrs, ports);
    lamport.Alg(filename, mode, iters);
}

TEST(Proc, Construct) {
	std::vector<char*> addrs;
	std::vector<int> ports;
    Proc proc("127.0.0.1", 4444, 0, addrs, ports);
}

TEST(Proc, Listen) {
	std::vector<char*> addrs;
	std::vector<int> ports;
    Proc proc("127.0.0.1", 4444, 0, addrs, ports);

    proc.Listen();	
}

// Тест на установку соединения
TEST(Proc, Connecting) {
	std::vector<char*> addrs;
	std::vector<int> ports;
    Proc proc("127.0.0.1", 4444, 0, addrs, ports);

    proc.Listen();
    std::thread t1([&] () {
		proc.Accept(0);
	});

    int sock = proc.Connect("127.0.0.1", 4444);
    close(sock);
    t1.join();
}

TEST(MutexTaking, SoloProc){
    std::vector<char*> addrs;
    std::vector<int> ports;
    Lamport lamport(0, "127.0.0.1", 4444, 0, addrs, ports);
    lamport.Alg("test.txt", 1, 1);
}

TEST(Lamport, TwoProc){
    int N = 2;
    std::string sad = "127.0.0.1";
    // addrs[0] = &sad[0];
    std::vector<char*> addrs(N-1, &sad[0]);
    // std::vector<int> ports(1);
    std::vector<std::thread> threads;
    // std::vector<Lamport> lamport;
    // Lamport lamport1()
    for(int i = 0; i < N; ++i){
        std::vector<int> ports;
        for(int j = 0; j < N; ++j){
            if(j != i) ports.push_back(4444+j);
        }
        // ports[0] = 4445 - i;
        threads.emplace_back([&, i, addrs, ports, N] () {
            f(i, "127.0.0.1", 4444+i, N-1, addrs, ports, "test.txt", 1, 10);
        });
        // lamport.emplace_back(i, "127.0.0.1", 4444 + i, 1, addrs, ports);
    }
    for(auto& thread : threads) thread.join();
}