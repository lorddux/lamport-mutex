#include "clock.h"

void Clock::Increase(){
	++time_;
}

void Clock::Compare(unsigned long long an_time){
	std::lock_guard<std::mutex> lock(mutex);
	if(an_time > time_){
		time_ = an_time;
	}
}

unsigned long long Clock::GetTime(){
	return time_;
}