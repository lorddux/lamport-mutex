#include <iostream>
#include <thread>

#include "lamport.h"

int main(int argc, char* argv[]) {
	int id = std::atoi(argv[1]);
	int mode = std::atoi(argv[2]);
	char* filename = argv[3];
	char* addr = argv[4];
	int port = std::atoi(argv[5]);
	unsigned proc_count = std::atoi(argv[6]);
	std::vector<char*> addrs;
	std::vector<int> ports;
	for(int i = 0; i < proc_count; ++i){
		addrs.push_back(argv[7+i*2]);
		ports.push_back(std::atoi(argv[8+i*2]));
	}
	int iters = atoi(argv[argc-1]);

	// alg(id, mode, filename, addr, port, proc_count, addrs, ports, iters);

	Lamport lamport(id, addr, port, proc_count, addrs, ports);
	lamport.Alg(filename, mode, iters);
	
    return 0;
}