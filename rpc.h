#pragma once

#include <sys/socket.h>
#include <thread>
#include <atomic>
#include <vector>
#include <chrono>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <set>
#include <stdexcept>
#include <iostream>
#include <mutex>
#include <sys/file.h>
#include <sys/time.h>
// #include <sys/types.h>
// #include <map>

#include "clock.h"

class Proc {
public:
	//создание сокета, заполнение полей
	Proc(const char* addr, int port, int proc_count, std::vector<char*> addrs, std::vector<int> ports); 
	//слушает входящие соединения
	void Listen();
	void Accept(int num);
	//подключение к серверу процесса
	int Connect(const char addr[], int port);
	// подключение ко всем другим процессам
	void ConnectToAll();
	// void sleep(unsigned milliseconds);
	// обработка запросов на взятие мьютекса
	// void ServerHandler(int client_sock);
	
	// возвращает логическое время процесса
	// unsigned long long Time();
	void SendPermission(int num, unsigned long long timestemp);
	std::pair<unsigned long long, int> RecvRequest(int num);
	// рассылка запроса на взятие мьютекса
	// unsigned long long MutexRequest();
	// отправка запроса на взятие мьютекса одному процессу. сериализация
	void SendRequest(int destnum, int myid, unsigned long long timestemp);
	// получение разрешения. десериализация
	unsigned long long RecvPermission(int sourcenum);
	// освобождение мьютекса
	void MutexRelease();
	// закрывает соединения с серверами других процессов
	void CloseSocks();
	void AddClSock(int num, int client_sock);

    ~Proc(){
    	close(sockfd);
    	// close(logfd);
    }
private:
	Clock p_time;
	int port;
	int sockfd;
	int proc_count;
	const char* addr;
	std::vector<char*> addrs;
	std::vector<int> ports;
	std::vector<int> socks;
	// int mode;
	int id;
	std::vector<int> client_socks;
	// int client_sock;
	// char* filename;
	// int logfd;
	// std::atomic<unsigned long long> rec_time;
};