#include "rpc.h"

Proc::Proc(const char* addr, int port, int proc_count, std::vector<char*> addrs, std::vector<int> ports) : 
    port(port), addr(addr), proc_count(proc_count),
    addrs(addrs), ports(ports)
    {
    if(proc_count == 0){
        client_socks.resize(1);
    } else {
        client_socks.resize(proc_count);
    }
    int opt = 1;
    sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    if (sockfd < 0) {
        throw std::runtime_error("Proc: socket error");
    }
}

void Proc::Listen() {
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( port );
    
    if(bind(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0) {
        close(sockfd);
        throw std::runtime_error("Server: bind failed. Error");
    }
    listen(sockfd, 16);
    printf("server started %s %d\n", addr, port);
}

// void Proc::ServeOne() {
//     std::vector<std::thread> threads;
//     for(int i = 0; i < proc_count; ++i){
//         int client_sock, c;
//         struct sockaddr_in client;
//         c = sizeof(struct sockaddr_in);
//         client_sock = accept(sockfd, (struct sockaddr *)&client, (socklen_t*) & c);
//         if (client_sock < 0) {
//             throw std::runtime_error("Server: Connection failed. Error");
//         }
        
//         threads.emplace_back([&, client_sock] () {
//             ServerHandler(client_sock);
//         });
//     }
//     for(auto& thread : threads) thread.join();
// }

void Proc::Accept(int num){
    int client_sock, c;
    // int c;
    struct sockaddr_in client;
    c = sizeof(struct sockaddr_in);
    client_sock = accept(sockfd, (struct sockaddr *)&client, (socklen_t*) & c);
    if (client_sock < 0) {
        throw std::runtime_error("Server: Connection failed. Error");
    }
    AddClSock(num, client_sock);
    // printf("Client connected\n");
    // return client_sock;
}

void Proc::SendPermission(int num, unsigned long long timestemp){
    std::string perm = std::to_string(timestemp);
    // request += " " + std::to_string(myid);
    if(send(client_socks[num], perm.c_str(), perm.size(), 0) < 0)
        throw std::runtime_error("send error");
}

std::pair<unsigned long long, int> Proc::RecvRequest(int num){
    char request[20];
    int read_size = recv(client_socks[num], request, 20, 0);
    // int read_size
    if(read_size < 0) {
        close(client_socks[num]);
        throw std::runtime_error("recv error");
    }
    if(read_size == 0) {
        close(client_socks[num]);
        // throw std::runtime_error("client disconnected");
        return std::make_pair(0, 0);
        printf("Client disconnected\n");
    }
    std::string s(request, read_size);
    int space_pos = s.find(" ");
    return std::make_pair(atoll(s.c_str()), atoi(s.substr(space_pos).c_str()));
}

void Proc::SendRequest(int destnum, int myid, unsigned long long timestemp){
    std::string request = std::to_string(timestemp);
    request += " " + std::to_string(myid);
    if(send(socks[destnum], request.c_str(), request.size(), 0) < 0)
        throw std::runtime_error("send error");
}

unsigned long long Proc::RecvPermission(int sourcenum){
    char perm[20];
    int read_size = recv(socks[sourcenum], perm, 20, 0);
    if(read_size < 0)
        throw std::runtime_error("recv error");
    std::string s(perm, read_size);
    return atoll(s.c_str());
}

// unsigned long long Proc::MutexRequest(){
//     timeval tv;
//     p_time.Increase();
//     rec_time = p_time.GetTime();
//     char log_mark[100];

//     gettimeofday(&tv, 0);
//     int len = sprintf(log_mark, "request PID %d real %ld.%ld log %lld\n", id ,tv.tv_sec, tv.tv_usec, p_time.GetTime());
//     if(write(logfd, log_mark, len) < 0) throw std::runtime_error("logging error");
    
//     for(auto sock : socks){
//         std::string request = std::to_string(rec_time);
//         request += " " + std::to_string(id);
//         write(sock, request.c_str(), request.size());
//     }
//     for(auto sock : socks) {
//         char perm[20];
//         int read_size = 0;
//         read_size = read(sock, perm, 20);
//         if(read_size <= 0){
//             printf("Connection closed\n");
//             for(auto s : socks) close(s);
//             return -1;
//         }
//         std::string s(perm, read_size);
//         unsigned long long an_time = atoll(s.c_str());
//         p_time.Compare(an_time);
//     }
//     p_time.Increase();
//     unsigned long long ac_time = p_time.GetTime();
    
//     gettimeofday(&tv, 0);
//     len = sprintf(log_mark, "acquare PID %d real %ld.%ld log %lld\n",id ,tv.tv_sec, tv.tv_usec, ac_time);
//     if(write(logfd, log_mark, len) < 0) throw std::runtime_error("logging error");
    
//     return ac_time;
// }

// void Proc::MutexRelease(){
//     // char log_mark[100];
//     // timeval tv;
    
//     // gettimeofday(&tv, 0);
//     // int len = sprintf(log_mark, "release PID %d real %ld.%ld log %lld\n",id ,tv.tv_sec, tv.tv_usec, p_time.GetTime());
//     // if(write(logfd, log_mark, len) < 0) throw std::runtime_error("logging error");
    
//     rec_time = -1; // переполняем rec_time
// }

// void Proc::ServerHandler(int client_sock){
//     char request[20];
//     int read_size = 0;
//     while((read_size = read(client_sock, request, 20)) > 0){
//         std::string s(request, read_size);
//         int space_pos = s.find(" ");
//         unsigned long long an_time = atoll(s.c_str());
//         int an_id = atoi(s.substr(space_pos).c_str());
//         p_time.Compare(an_time);
//         p_time.Increase();
//         unsigned long long g = rec_time;
//         while(!((rec_time > an_time) || (rec_time == an_time && id > an_id))){
//         }
//         read_size = sprintf(request, "%lld %d", p_time.GetTime(), id);
//         write(client_sock, request, read_size);
//     }
// }

void Proc::ConnectToAll(){
    for(int i = 0; i < proc_count; ++i){
        socks.push_back(Connect(addrs[i], ports[i]));
    }
}

int Proc::Connect(const char addr[], int port){
    int sock;
    struct sockaddr_in server;
    sock = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP);
    if (sock < 0) {
        throw std::runtime_error("Client: socket error");
    }
    std::string compare_str(addr);
    if(compare_str == "localhost"){
        server.sin_addr.s_addr = inet_addr("127.0.0.1");    
    } else {
        server.sin_addr.s_addr = inet_addr(addr);
    }
    server.sin_family = AF_INET;
    server.sin_port = htons( port );
 
    while(connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0) {
    }
    return sock;
}

void Proc::AddClSock(int num, int client_sock){
    client_socks[num] = client_sock;
}

void Proc::CloseSocks(){
    for(auto s : socks) close(s);
}

// unsigned long long Proc::Time(){
//     return p_time.GetTime();
// }