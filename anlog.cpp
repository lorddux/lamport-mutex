#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <algorithm>

unsigned long long gettime(char buf[]){
	std::string line(buf);
	return atoll(line.substr(line.find("log") + 4).c_str());
}

struct AcqRel{
	int pid = 0;
	unsigned long long line = 0;
	unsigned long long acq = 0;
	unsigned long long rel = 0;
};

bool compare_acq(AcqRel el1, AcqRel el2){
	return el1.acq < el2.acq;
}

bool compare_acqrel(AcqRel el1, AcqRel el2){
	return el1.rel < el2.acq;
}

int main(int argc, char* argv[]){
	if(argc < 2) {
		printf("type proc id\n");
		return 0;
	}

	bool flag = false;

	FILE *fp;
	unsigned long long line_num = 0;
	std::vector<AcqRel> all_acq_req;
	for(int i = 1; i < argc; ++i){
		std::string filename(argv[i]);
		filename += "_log.txt";
		if((fp = fopen(filename.c_str(), "rt")) == NULL){
			printf("cannot open file %s\n", filename.c_str());
			return 0;
		}
		while(true){
			AcqRel acqrel;
			acqrel.pid = atoi(argv[i]);
			char buf[100];
			char w1[7];
			char w2[7];
			char w3[7];
			std::string s;
			fscanf(fp, "%s", w1);
			++line_num;
			s.assign(w1);
			if(s != "request") {
				flag = true;
				printf("PID %d line %lld: expected 'request' instead of '%s'\n", acqrel.pid, line_num,  s.c_str());
			}
			if(fgets(buf, 100, fp) == NULL)
				break;
			unsigned long long request_time = gettime(buf);
			fscanf(fp, "%s", w2);
			s.assign(w2);
			++line_num;
			if(s != "acquare") {
				flag = true;
				printf("PID %d line %lld: expected 'aqcuare' instead of '%s'\n", acqrel.pid, line_num,  s.c_str());
			}
			fgets(buf, 100, fp);
			acqrel.acq = gettime(buf);
			if(acqrel.acq < request_time) {
				flag = true;
				printf("PID %d line %lld: acquare time < request time\n", acqrel.pid, line_num);
			}
			acqrel.line = line_num;
			fscanf(fp, "%s", w3);
			s.assign(w3);
			++line_num;
			if(s != "release") {
				flag = true;
				printf("PID %d line %lld: expected 'release' instead of '%s'\n", acqrel.pid, line_num,  s.c_str());
			}
			if(fgets(buf, 100, fp) == NULL)
				break;
			acqrel.rel = gettime(buf);
			all_acq_req.push_back(acqrel);
		}
	}
	std::sort(all_acq_req.begin(), all_acq_req.end(), compare_acq);
	for(int i = 0; i < all_acq_req.size() - 1; ++i){
		if(!compare_acqrel(all_acq_req[i], all_acq_req[i+1])){
			flag = true;
			printf("Acquire-release crossed. PID %d line %lld, PID %d line %lld\n", all_acq_req[i].pid, all_acq_req[i].line, all_acq_req[i+1].pid, all_acq_req[i+1].line);
		}
	}
	// if(v != all_acq_req) flag = true;
	if(flag) printf("NOT OK\n");
	else printf("OK\n");
	return 0;
}